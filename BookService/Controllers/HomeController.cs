﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult AddBook()
        {
            return View();
        }

        public ActionResult Library()
        {
            return View();
        }

        public ActionResult HomePage()
        {
            return View();
        }
    }
    }

